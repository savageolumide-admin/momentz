import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ChatScreen from '../screens/NotificationScreen/ChatScreen';
import MessageScreen from '../screens/NotificationScreen/MessagesScreen';
import NotificationScreen from '../screens/NotificationScreen/index';

const NotificationStack = createStackNavigator();

export default NotificationStackNavigator = () => {
  return (
    <NotificationStack.Navigator initialRouteName={'NotificationScreen'}>
      <NotificationStack.Screen
        options={{headerShown: false}}
        name="NotificationScreen"
        component={NotificationScreen}
      />
      <NotificationStack.Screen
        options={{headerShown: false}}
        name="ChatScreen"
        component={ChatScreen}
      />

      <NotificationStack.Screen
        options={{headerShown: false}}
        name="MessageScreen"
        component={MessageScreen}
      />
    </NotificationStack.Navigator>
  );
};
