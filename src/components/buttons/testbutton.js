import React, {useRef,useEffect} from 'react';
import {Text, View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import { color } from 'react-native-reanimated';
import EvilIcons from 'react-native-vector-icons/dist/EvilIcons';

// function CircularButton({
//   radius = 50,
//   margins = {},
//   bgcolor,
//   textColor = '#FCFCFC',
//   btnText = 'Text',
// }) {
  export default function CircularButton(props) {
    // const [bgcolor, setbgcolor] = React.useState('');
    
const margins={};
const textColor = '#FCFCFC';
const  btnText = 'Text';
const  bgcolor  = props.backcolor;
const radius = props.radius;
const styles = getStyles(bgcolor, radius);
console.log("props here:",radius);
// useEffect(() => {
//    setbgcolor(props.backcolor+"");
// }, []);
// console.log(bgcolor);
  return (
    // style={[
    //   styles.btnContainer,
    //   {
    //     height: radius * 2,
    //     width: radius * 2,
    //     borderRadius: radius,
    //     backgroundColor:props.backcolor,

    //   },
    //   margins
    // ]}
    <TouchableOpacity style={styles.container}>
      
      <EvilIcons name={'image'} size={30} color={textColor}/>

      <Text style={[styles.buttonText, {color: textColor}]}>{btnText}</Text>
    </TouchableOpacity>
  );
}



const getStyles = (bgcolor, radius) => StyleSheet.create({
  btnContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  container:{
    height: radius * 2,
    width: radius * 2,
    borderRadius: radius,
    backgroundColor: `${bgcolor}`,
  },
  buttonText: {
    fontSize: 18,
    lineHeight: 34,
    textAlign: 'center',
    color: '#287287',
  },
});

