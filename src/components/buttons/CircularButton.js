import React from "react";
import {Text, View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import { color } from 'react-native-reanimated';
import EvilIcons from 'react-native-vector-icons/dist/EvilIcons';

export default class CircularButton extends  React.Component {
  constructor(props) {
    super(props);
    this.state = {
      btnText:this.props.btnText,
      bgcolor:this.props.backcolor,
      radius:this.props.radius,
      margins:this.props.margins,
      // interest:this.props.interests,
    };
     // console.log("margins",this.state.margins.marginTop);
    console.log("props here:",this.props.backcolor);
  }
  //  textColor = '#FCFCFC';
  // btnText = this.props.btnText;
  //   bgcolor  = this.props.backcolor;
  //  radius = this.props.radius;
  // const styles = getStyles(bgcolor, radius);
  

  
  onPressHandler = (key,name) => event => {
   // console.log("name",name);
    this.props.circularButtonClick(key,name);
    if(this.state.bgcolor === '#23395d')
    {
        this.setState({
          bgcolor: 'green'
      });
    }
    else{
        this.setState({
          bgcolor: '#23395d'
      });
    }
   
  
 }

  render() {
    return (
  
        <TouchableOpacity  onPress = {this.onPressHandler(this.props.ids,this.props.name)}  
        style={[styles.container,{backgroundColor:this.state.bgcolor,marginTop: this.props.marginTop, 
        marginLeft: this.props.marginLeft,height:this.props.radius * 2,width:this.props.radius * 2,
        borderRadius:this.props.radius}]} >

        <EvilIcons name={'image'} size={50} color='#FCFCFC'   />
        <Text style={[styles.buttonText, {color:'#FCFCFC'}]}>{this.state.btnText}</Text>

        </TouchableOpacity>
     
    )
  }
  
}
const styles = StyleSheet.create({
  btnContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  container:{
    justifyContent: 'center',
    alignItems: 'center',
    // height:50 * 2,
    // width: 50 * 2,
    // borderRadius: 50,
    
  },
  buttonText: {
    fontSize: 18,
    lineHeight: 34,
    textAlign: 'center',
    color: '#ffffff',
  },
  
});


