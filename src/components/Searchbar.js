import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
const Searchbar = ({placeholder = 'Search events'}) => {
  return (
    <View style={styles.container}>
      {/* <AntDesignIcon name="search1" style={styles.icon} /> */}
      {/* <TextInput style={styles.textInput} placeholder={placeholder} /> */}
        <GooglePlacesAutocomplete
         styles={{
          textInputContainer: {
            backgroundColor: 'grey',
          },
          textInput: {
            height: 38,
            color: '#5d5d5d',
            fontSize: 16,
          },
          predefinedPlacesDescription: {
            color: '#1faadb',
          },
        }}
        minLength={2}
        autoFocus={false}
        returnKeyType={'default'}
          placeholder='Search'
          onPress={(data, details = null) => {
            // 'details' is provided when fetchDetails = true
            console.log(data, details);
          }}
          query={{
            key: 'AIzaSyB2rtoExWWydu6QR6rtg6ebOlHKfjZL9io',
            language: 'en',
          }}
          debounce={400}
          nearbyPlacesAPI="GooglePlacesSearch"
    />
    </View>
  );
};

export default Searchbar;

const styles = StyleSheet.create({
  container: {
    position: 'relative',
  },
  icon: {
    position: 'absolute',
    top: 10,
    left: 20,
    fontSize: 30,
    color: 'black',
    zIndex: 10,
  },
  textInput: {
    fontSize: 20,
    backgroundColor: '#fff',
    borderRadius: 30,
    paddingLeft: 65,
    paddingRight: 10,
    paddingVertical: 12,
  },
});
