import React from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Dimensions,
} from 'react-native';
import GradientButton from '../components/buttons/GradientButton';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'; 
import { Marker } from 'react-native-maps';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import Colors from '../constants/Colors.js';
import HeaderWithSearchbar from '../components/Header/HeaderWithSearchbar';

export default ChangeLocationScreen = ({navigation}) => {




  
  return (
    <View style={styles.container}>
      
      <HeaderWithSearchbar
        headerStyle={styles.headerStyle}
        showtagline
        title={'Where are you partying?'}
        tagLine={
          'You can change locations to find or plan parties in other states'
        }
        searchplaceholder={"Search locations"}
      />
      <MapView tyle={styles.mapContainer}
      //  provider={PROVIDER_GOOGLE}
        style={{flex: 1,zIndex:-1}}
        initialRegion={{
          latitude: 26.4499,
          longitude: 80.3319,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
      >
        <Marker 
        coordinate={{ latitude : 26.4499 , longitude : 80.3319 }}
        title="hello"
        description="yeh"
        // image={{uri: 'custom_pin'}}
      />
        </MapView>
       {/* <GooglePlacesAutocomplete
       styles={
         {
           container:{
             flex:1,
           },
           textInput:{
             fontSize:18
           }
         }
       }
          placeholder='Search'
          onPress={(data, details = null) => {
            // 'details' is provided when fetchDetails = true
            console.log(data, details);
          }}
          query={{
            key: 'AIzaSyB2rtoExWWydu6QR6rtg6ebOlHKfjZL9io',
            language: 'en',
          }}
          debounce={400}
          nearbyPlacesAPI="GooglePlacesSearch"
    /> */}

      <View style={styles.btnContainer}>
        <GradientButton
          btnStyles={{
            width: '80%',
          }}
          onPress={() => navigation.navigate('EventDashboard')}
          // onPress={() => navigation.navigate('CreateTicketsScreen')}

          btnText={'Set Location'}
        />
        <TouchableOpacity
        onPress={()=>console.log("pressed")}
         style={styles.locationBtn}>
          <MaterialIcons name={'my-location'} size={40} color={'#FFF'} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // ...StyleSheet.absoluteFillObject,
    flex: 1,
  },
  mapContainer:{
    // ...StyleSheet.absoluteFillObject,
  },
  btnContainer: {
    position: 'absolute',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingHorizontal:20,
    bottom: 0,
  },
  locationBtn: {
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:Colors.violetDark
  
  },
  headerStyle: {
    position: 'relative',
    top: 0,
    width: '100%',
    justifyContent: 'flex-end',
    zIndex:1
  },
});
