import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import validator from 'validator'
import {
  ticketFormService,
} from '../../services/authService';
// import {addUser} from './../redux/actions/userActions';
import RBSheet from 'react-native-raw-bottom-sheet';
export const useTicketService = () => {
  const dispatch = useDispatch();
  const [loading, setLoading] = React.useState(false);
  const [nameValidation, setnameValidation] = React.useState(false);
  const [descriptionValidation, setdescriptionValidation] = React.useState(false);
  const [priceValidation, setpriceValidation] = React.useState(false);
  const [stockValidation, setstockValidation]= React.useState(false);
  const [validation, setvalidation] = React.useState(true);
  const [ticketResult, setticketResult] = React.useState(false);
  const refRBSheet = React.useRef();

  const [registerForm, setRegisterForm] = React.useState({
    name: '',
    description: '',
    stock: '',
    price: '',
  });

  // const [registerForm, setRegisterForm] = React.useState({ });
  function validateName(name,text)
 {
        if(name=="name")
        {
                if(text.length >1){
                console.log("Is name strong");
                setnameValidation(true);
              } else {
                console.log("Is Not Strong name");
                setnameValidation(false);
              }
        }
 }
 function validateDescriptionr(name,text)
 {
          if(name=="description")
          {
                  if(text.length > 1){
                  console.log("Is description strong");
                  setdescriptionValidation(true);
                } else {
                  console.log("Is Not Strong name");
                  setdescriptionValidation(false);
                }
              
        }
}
function validatePrice(name,text)
 {
          if(name=="price" )
          {
            if (isNaN(text)  ) {
              console.log("not valid number");
              setpriceValidation(false);
            }      
            else{
              setpriceValidation(true);
           
            }
        }
}
function validateStock(name,text)
 {
          if(name=="stock" )
          {
            if (isNaN(text )  ) {
              console.log("not aa valid number");
              setstockValidation(false);
            }      
            else{
              setstockValidation(true);
              // setstockValidation(true);
            }
        }
}


  const handleTicketFormChange = (name, text) => {
   console.log("text here:",text);
    validateName(name,text);
    validateDescriptionr(name,text);
    validatePrice(name,text);
    validateStock(name,text);

    
      setRegisterForm({
        ...registerForm,
        [name]: text,
      });
    
    
    
  };
 const handleTicketFormSubmit  = async () => 
 {
   const eventTypeId="1";
        setLoading(true);
            if(registerForm.name=="" || registerForm.description=="" || registerForm.price=="" || registerForm.stock=="")
            {
              setvalidation(false);
              console.log("not proper  valid ");
              setLoading(false);
              return ;
            }
        if(priceValidation == false || stockValidation == false || descriptionValidation == false || nameValidation == false)
        {
          setvalidation(false);
          console.log("not proper  valid ");
          setLoading(false);
          return ;
        }
      try {
        console.log("inside try");
        const response = await ticketFormService(registerForm,eventTypeId);
        // setsignupResult(true);
       
        

        console.log("after ticket form submit",response.data);
       // setcustomerId(response.data.customerId);
       //setticketResult(true);
       refRBSheet.current.open();

      //   setTimeout(() => {
      //   //setLoading(false);
      // }, 2000);
      } catch (error) {
        console.log('Auth Eror --- ticket-------');
        console.log(error.message);
       // setLoading(false);
       // setticketResult(false);

      }
 }

 
  return {
    loading,
    setLoading,
    handleTicketFormChange,
    handleTicketFormSubmit,
    registerForm,
    validation,
    setvalidation,
    ticketResult,
    refRBSheet

  };
};
