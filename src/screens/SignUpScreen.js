import React, {useRef,useEffect,useState} from 'react';
import {Picker} from '@react-native-community/picker';
import DatePicker from 'react-native-datepicker'

// import { NavigationActions } from 'react-navigation';
import {
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  Alert,
  Button ,
  BackHandler,
  
} from 'react-native';
import FormTextInput from '../components/formInputs/FormTextInput';
import GradientButton from '../components/buttons/GradientButton';
import TransParentButton from '../components/buttons/TransParentButton';
import RBSheet from 'react-native-raw-bottom-sheet';
import OtpInput from '../components/OtpInput';
import {useDispatch} from 'react-redux';
import {useAuthService} from './../hooks/useAuthService';
import {addUser} from './../redux/actions/userActions';
import { TouchableOpacity } from 'react-native-gesture-handler';
import CountryCodes from '../data/countries'

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default SignUpScreen = ({navigation}) => {
  const {validation,setValidation,signupResult,setsignupResult,handleRegisterFormChange,handleRegisterFormSubmit,
     handleRequestOTP,otp, setOtp,setopenOTPScreen,setRegisterForm,invalidOtp,isOtpVerified
     ,otpformatRight,customerId,setotpformatRight,setinvalidOtp, openOTPScreen,registerForm,setcountryname
     ,setcountrycode,countryname,setDate,date} =
    useAuthService();
    // const [countrycode, setcountrycode] = useState();
    // const [countryname, setcountryname] = useState();
    const [pickerValue, setpickerValue] = useState();
   
    
    console.log("date:",date);
  
    if(validation == false)
    {
        Alert.alert(
            "Validation Error:",
            "Fields are not validated properly!!",
            [
              {
                
              },
              { text: "OK", onPress: () => console.log("OK Pressed") }
            ]
          );

          setValidation(true);
    }
    if(signupResult == false)
    {
      Alert.alert(
        "signup failed:",
        "User already Exists!!",
        [
          {
            
          },
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
      );
      setsignupResult(true);
    }
    if(invalidOtp == true)
    {
      Alert.alert(
        "signup failed:",
        "Invalid OTP!!",
        [
          {
            // text: "Cancel",
            // onPress: () => console.log(""),
            // style: "cancel"
          },
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
      );
      setinvalidOtp(false);
      setOtp("");
      
    }
    if(otpformatRight == true)
    {
      console.log("navigate here");
      navigation.navigate('CreateProfileScreen',{ customer_id: customerId,country_name:countryname })  
      // const resetAction = NavigationActions.reset({
      //   index: 0,
      //   actions: [NavigationActions.navigate({ routeName: 'CreateProfileScreen' })],
      // });
      // navigation.dispatch(resetAction);
     
    }
    const textInputRef = React.useRef();
    useEffect(() => {
      // Subscribe for the focus Listener
      // const interval = setInterval(() => {
      //   setCount((count) => count + 1);
      // }, 1000);
   
     
      const unsubscribe = navigation.addListener('focus', () => {
        setOtp("");
        setotpformatRight(false);
        setRegisterForm({
          email: '',
          password: '',
          phoneNumber: '',
          dob: '',
        });
   
      });
      return () => {
        // Unsubscribe for the focus Listener
        // clearTimeout(interval);
        unsubscribe;
      };
    }, [navigation]);
    const backAction = () => {
      navigation.navigate('LoginScreen');
      return true;
    };
     
    // Calender functionality
    const [isPickerShow, setIsPickerShow] = useState(false);
    
  
    const showPicker = () => {
      setIsPickerShow(true);
    };
  
    const onChange = (event, value) => {
      //setDate(value);
      if (Platform.OS === 'android') {
        setIsPickerShow(false);
      }
    };
  

    useEffect(() => {
      BackHandler.addEventListener("hardwareBackPress", backAction);
  
      return () =>
        BackHandler.removeEventListener("hardwareBackPress", backAction);
    }, []);
  const dispatch = useDispatch();

  const refRBSheet = useRef();

  // const handleSignUp =() => {
  //   // if (await handleRequestOTP()) {
  //   //   openOtpInput();
  //   // }
  //   // openOtpInput();
  //   // dispatch(
  //   //   addUser({
  //   //     token: 'dsa',
  //   //   }),
  //   // );
  //   navigation.navigate('CreateProfileScreen');  
  // };

  // const openOtpInput = () => refRBSheet.current.open();
  if(openOTPScreen)
  {
    //  openOtpInput();
    refRBSheet.current.open();
    setopenOTPScreen(false);
  }
  // onDateChange(e)
  // {
  //   conole.log("date",e.target.value);
  // }
  return (
    <ScrollView
      style={{flex: 1}}
      showsVerticalScrollIndicator={false}
      contentContainerStyle={{flexGrow: 1}}>
      <View style={styles.container}>
        <View style={{height: windowHeight * 0.2}}>
          <Image
            style={{
              height: '100%',
              width: '30%',
            }}
            source={require('../assets/momentz_final.png')}
          />
        </View>
        <View style={{flex: 0.75}}>
          <Text style={styles.headigText}>Join The Community</Text>
          <Text style={[styles.headigText, {fontSize: 13, lineHeight: 22}]}>
            and 1,254,5797 other users having a good time.
          </Text>
          <View style={styles.formCoantainer}>
            <FormTextInput
             
              name="email"
              onChangeText={handleRegisterFormChange}
              placeholder={'Email'}
              value={registerForm.email}
              keyboardType= "email-address"
            />
            <FormTextInput
              name="password"
              onChangeText={handleRegisterFormChange}
              placeholder={'Password'}
              value={registerForm.password}
            />
            <FormTextInput
              name="phoneNumber"
              onChangeText={handleRegisterFormChange}
              placeholder={'Phone Number'}
              value={registerForm.phoneNumber}
              keyboardType= "numeric"
            />
             <View style={[styles.container1, ]}>
             <Picker 
            selectedValue={pickerValue}
            onValueChange={(value, key) => 
              {
                setpickerValue(value);
                setcountrycode(CountryCodes[key].dialCode);
                setcountryname(CountryCodes[key].name);
              }
           
            } >
            { CountryCodes.map((item, key)=>(
            <Picker.Item label={item.name} value={item.name}  key={item.name} >
               
                </Picker.Item>
             
            )
            )}

          </Picker>
          </View >
            {/* <TouchableOpacity 
             activeOpacity={0.2}
             onPress={()=> setIsPickerShow(true)}>
            <FormTextInput
              name="dob"
              keyboardType="DOB"
              // value={registerForm.dob}
              // onChangeText={handleRegisterFormChange}
              // keyboardType="datetime"
              // placeholder={'Date of Birth'}
              value={date.toUTCString()}
              onChangeText={handleRegisterFormChange}
              placeholder={'Date of Birth'}
              editable = {false}
            />
            </TouchableOpacity> */}
              {/* <FormTextInput
              name="dob"
              keyboardType="DOB"
              // value={registerForm.dob}
              // onChangeText={handleRegisterFormChange}
              // keyboardType="datetime"
              // placeholder={'Date of Birth'}
              value={registerForm.dob}
              onChangeText={handleRegisterFormChange}
              placeholder={'Date of Birth'}
              
            /> */}
             <DatePicker
              style={{width: 200}}
              date={date}
              mode="date"
              placeholder="select date"
              format="YYYY-MM-DD"
              minDate="2016-05-01"
              maxDate="2050-06-01"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 36
                }
                // ... You can check the source to find the other keys.
              }}
              onDateChange={(date) => {setDate(date)}}
            />
          </View>
          
          {/* {isPickerShow && (
             <DateTimePicker
               value={date} // Initial date from state
               mode="date" // The enum of date, datetime and time
               placeholder="select date"
               format="DD-MM-YYYY"
               confirmBtnText="Confirm"
               cancelBtnText="Cancel"
               display={Platform.OS === 'ios' ? 'spinner' : 'default'}
               onChange={onChange}
             />
           )} */}
        </View>
        <GradientButton onPress={handleRegisterFormSubmit} btnText={'Sign Up'} />
        <TransParentButton
          onPress={() => navigation.navigate('LoginScreen')}
          btnText={'Log In'}
        />
        {/* <TransParentButton
          onPress={() => navigation.navigate('CreateProfileScreen',{ customer_id: "1" })}
          btnText={'Profile page'}
        /> */}
      </View>
      {/* <Button title="OPEN BOTTOM SHEET" onPress={() => refRBSheet.current.open()} /> */}
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        height={350}
        closeOnPressMask={false}
        customStyles={{
          draggableIcon: {
            backgroundColor: 'blue',
          },
        }}>
        <OtpInput setOtp={setOtp} otp={otp} phoneNumber={registerForm.phoneNumber}/>
      </RBSheet>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#FFF',
  },
  container1: {
    backgroundColor: '#EFF0F6',
    borderRadius: 15,
    marginVertical: 10,
    height: 50,
    
  },
  headigText: {
    fontSize: 48,
    lineHeight: 50,
    color: '#14142B',
    fontWeight: '300',
  },
  formCoantainer: {
    marginTop: 20,
  },
});

