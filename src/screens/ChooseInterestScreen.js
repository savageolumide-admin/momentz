import React, {useRef,useEffect,useState} from 'react';
import {Text, View, ScrollView, StyleSheet, Dimensions,Alert,BackHandler} from 'react-native';
import GradientButton from '../components/buttons/GradientButton';
import CircularButton from '../components/buttons/CircularButton';
import {useNavigation} from '@react-navigation/native';
import {useProfileService} from './../hooks/useProfileService';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

// const btnConfig = [
//   [
//     {id:1,radius: 50, name: 'Music', margins: {marginTop: 10, marginLeft: 10},bgColor:'#ff00ff'},
//     {id:2,radius: 50, name: 'Sports', margins: {marginTop: 10, marginLeft: 50},bgColor:'#ff00ff'},
//   ],
//   [
//     {id:3,radius: 50, name: 'Games', margins: {marginTop: 50, marginLeft: 10},bgColor:'#ff00ff'},
//     {id:4,radius: 60, name: 'Eating', margins: {marginTop: 10, marginLeft: 10},bgColor:'#ff00ff'},
//     {id:5,radius: 85, name: 'Dancing', margins: {marginTop: 10, marginLeft: 10},bgColor:'#ff00ff'},
//   ],
//   [
//     {id:6,radius: 70, name: 'Tech', margins: {marginTop: 10, marginLeft: 10},bgColor:'#ff00ff'},
//     {id:7,radius: 60, name: 'Dancing', margins: {marginTop: -30, marginLeft: 10},bgColor:'#ff00ff'},
//     {id:8,radius: 85, name: 'Eating', margins: {marginTop: 10, marginLeft: 10},bgColor:'#ff00ff'},
//   ],
  
  
  
//   // [{radius: 60, name: 'Sports', margins: {marginTop: -50, marginLeft: -100}}],
// ];

  const btnConfig = [
    [
      {id:1,radius: 50, name: 'Music',marginTop: 10, marginLeft: 10,bgColor:'#23395d'},
      {id:2,radius: 50, name: 'Sports',marginTop: 10, marginLeft: 50,bgColor:'#23395d'},
    ],
    [
      {id:3,radius: 50, name: 'Games', marginTop: 50, marginLeft: 10,bgColor:'#23395d'},
      {id:4,radius: 60, name: 'Eating', marginTop: 10, marginLeft: 10,bgColor:'#23395d'},
      {id:5,radius: 85, name: 'Dancing',marginTop: 10, marginLeft: 10,bgColor:'#23395d'},
    ],
    [
      {id:6,radius: 70, name: 'Tech', marginTop: 10, marginLeft: 10,bgColor:'#23395d'},
      {id:7,radius: 60, name: 'Dancing',marginTop: -30, marginLeft: 10,bgColor:'#23395d'},
      {id:8,radius: 85, name: 'Eating', marginTop: 10, marginLeft: 10,bgColor:'#23395d'},
    ],
  
  
  
  // [{radius: 60, name: 'Sports', margins: {marginTop: -50, marginLeft: -100}}],
];
function ChooseInterestScreen({route,navigation}) {
  const { customer_id } = route.params;
  //console.log("cusomer id",customer_id);
  //const navigation = useNavigation();
  const {interest,setinterest,handleInterestFormSubmit,setcustomerid,interestResult} = useProfileService();

  const [btnData, setbtnData] = useState(btnConfig);
  const [selectedItem, setselectedItem] = useState([]);
  const [forcestatechange, setforcestatechange] = useState(false);
   
  useEffect(() => {
    
    console.log("hey");
  }, [btnData]);

  useEffect(() => {
     setcustomerid(customer_id);
    // setbtnData(btnConfig);
  }, []);

   const backAction = () => {
    navigation.navigate('LoginScreen');
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backAction);

    return () =>
      BackHandler.removeEventListener("hardwareBackPress", backAction);
  }, []);

  
  // const buttonColorhandlechange = (buttonId,name) => {
      
  //  // setforcestatechange(true);
  //    console.log("button id here:",buttonId);

  //       const filteredExpenses = btnData.filter((element) => {
         
  //         const button = element.find((btn) => {
  //               if( btn.id === buttonId)
  //               {  return btn;}
  //           });
  //           return button;
  //         });
  //      //   console.log("filter:",filteredExpenses);
  //   //       const findIndex = btnData.map((element) => {
  //   //         // console.log("filter",btn);
  //   //         const button = element.findIndex((btn,index) => {
             
  //   //               if( btn.id === buttonId)
  //   //               {  return index;}
  //   //           });
  //   //           return button;
  //   //         });
  //   //         console.log("index:",findIndex);
  //   //       console.log("filter:",filteredExpenses);

  //         const button = filteredExpenses.map((btn,index) => {  
           
  //               for(var i=0;i<btn.length;i++)
  //               {
  //                 if(btn[i].id === buttonId)
  //                 {  
  //                   console.log("id",btn[i].id);
                    
  //                     setbtnData((prevExpenses) => {
  //                 prevExpenses[0][0].bgColor='#ff0000';
  //                // console.log("prevExpenses:", prevExpenses);
  //                const expenses=prevExpenses;
  //                 // setforcestatechange(true);
                   
  //                 return [...expenses];
                  
  //               });
                
  //                // break;
                  
  //               };
              
  //               }   
  //           });
  //         // console.log("selected item",selectedItem);
  //           // setbtnData((prevExpenses) => {
  //           //   prevExpenses[0][0].bgColor='#ff0000';
  //           //   console.log("prevExpenses:", prevExpenses);
  //           //   return [prevExpenses];
  //           // });
              
  // }
   console.log("btnData",btnData);
  // useEffect(() => { 
   
  //   console.log("btnData",btnData);
  //   console.log("btnData",forcestatechange);
  // }, [btnData,forcestatechange]);
  // // useEffect(() => {
   
  // //   const unsubscribe = navigation.addListener('focus', () => {
  // //     setbtnData(btnConfig);
  // //   });
  // //   return () => {
      
  // //     unsubscribe;
  // //   };
  // // }, [navigation]);
       
   const buttonColorhandlechange = (buttonId, name) => {
    console.log("button id here:",buttonId);
    // Alert.alert(
    //   "You have selected "+name+" as interest",
    //   "",
    //   [
    //     {
    //       text: "",
    //       onPress: () => console.log("Cancel Pressed"),
    //       style: "cancel"
    //     },
    //     { text: "OK", onPress: () => console.log("OK Pressed") }
    //   ]
    // );
  
     const arr=[buttonId,...interest];
    if(interest.includes(buttonId))  
    {  
     const new_interest = interest.filter(item => item !== buttonId)
            console.log("checking point here:");
              setinterest(new_interest);
    }  
    else  
    {  
      setinterest(arr);
       // setinterest([...new Set(arr)]);
    }  

   }
   if(interestResult)
   {
    navigation.navigate('LoginScreen');
   }
console.log("interset here:",interest);
  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={{backgroundColor: '#FFF'}}>
      <View style={styles.container}>
        <Text   style={[styles.headingText, {color:'#000000'}]}>Choose Interests</Text>
        <View
          style={{
            flex: 1,
            height: windowHeight * 0.8,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          {btnData.map((btn,index) => (
            <View style={{flexDirection: 'row'}}>
              {btn.map((b,index) => (
                <CircularButton
                  ids={b.id}
                  // item={selectedItem}
                  // interests={interest}
                  radius={b.radius}
                  // margins={b.margins}
                  name={b.name}
                  margintop={b.marginTop}
                  marginleft={b.marginLeft}
                  btnText={b.name}
                  backcolor={b.bgColor}
                  circularButtonClick={buttonColorhandlechange}
                />
              ))}
            </View>
          ))}
        
          
        </View>
        <GradientButton
          colors={['#00BA88', '#00BA88']}
          btnText={'To Explore Page'}
          onPress={handleInterestFormSubmit}
        />
      </View>
    </ScrollView>
  );
}

export default ChooseInterestScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    padding: 20,
  },
  headingText: {
    fontSize: 24,
    lineHeight: 28,
    color: '#4E4B66',
    textAlign: 'center',
    marginTop: 20,
  },
});
