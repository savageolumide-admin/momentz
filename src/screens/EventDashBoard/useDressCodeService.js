import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {
  dresscodeFormService,
} from '../../services/authService';
import {
  Alert,
} from 'react-native';



export const useDressCodeService = () => {
 // const dispatch = useDispatch();
  const [colorOne, setcolorOne]= React.useState("");
  const [colorTwo, setcolorTwo]= React.useState("");
  const [displayColorComponent,setdisplayColorComponent] = React.useState(false);
  const [selectedid,setselectedid] = React.useState("");
  const [check, setcheck] = React.useState("yes");


  const [registerForm, setRegisterForm] = React.useState({
    dressCodeCategoryId: '',
    colorOne: '',
    colorTwo: '',
    description: '',
  });

  const handleAddColor1Change = (name, text) => {
   console.log("text here:",text);  
    if(name=="colorOne")
    {setcolorOne(text);}
      setRegisterForm({
        ...registerForm,
        [name]: text,
      });
      
  };
  const handleAddColor2Change = (name, text) => {
    console.log("text here:",text);
     if(name=="colorTwo")
     {setcolorTwo(text);}
       setRegisterForm({
         ...registerForm,
         [name]: text,
       });
   };
  const handleAddColorText = () => {
    if(colorOne.length>1)
    {setdisplayColorComponent(true);
    }  
   };
   
   const handledescription = (name,text) => {
    setRegisterForm({
        ...registerForm,
        [name]: text,
      });  
   };
   //dress code form submit
 const handleDressCodeFormSubmit  = async () => 
 {
   const eventTypeId="1";
 console.log("dress code:",check);
   console.log("register form:",registerForm);
   try {
    console.log("inside try");
    const response = await dresscodeFormService(registerForm,eventTypeId);
    console.log("after ticket form submit",response.data);
    showAlert("Sucessfully uploaded dresscode!!");

  } catch (error) {
    console.log('Auth Eror --- ticket-------');
    console.log(error.message);
    showAlert("Something went wrong!!please retry");

  }  
 }
function showAlert(message)
{
  Alert.alert(
    ""+message,
    "",
    [
      {},
      { text: "OK", onPress: () => console.log("OK Pressed") }
    ]
  );
}
 
  return {
    registerForm,
    setRegisterForm,
    handleAddColor1Change,
    handleAddColor2Change,
    handleAddColorText,
    handleDressCodeFormSubmit,
    displayColorComponent,
    handledescription,
    selectedid, 
    setselectedid,
    setcheck,
    check,
  };
};
