import React, {useRef,useEffect} from 'react';
import {Text, View, Dimensions, StyleSheet, TouchableOpacity, ScrollView,Alert,BackHandler} from 'react-native';
import FormInputWithTitle from '../components/formInputs/FormInputWithTitle';
import GradientButton from '../components/buttons/GradientButton';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import EvilIcons from 'react-native-vector-icons/dist/EvilIcons';
import LinearGradient from 'react-native-linear-gradient';
import {useProfileService} from './../hooks/useProfileService';
import Geolocation from '@react-native-community/geolocation';
import FormTextInput from '../components/formInputs/FormTextInput';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default CreateProfileScreen = ({route,navigation}) => {

  const {handleProfileFormChange,handleProfileFormSubmit,profileForm,profileResult,setcustomerid,
    Validation,setValidation} = useProfileService();

  const { customer_id } = route.params;
  const { country_name } = route.params;

  console.log("navigation:",customer_id);
  if(Validation == true)
  {
    Alert.alert(
      "Validation Error:",
      "Fields are not validated properly!!",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log("OK Pressed") }
      ]
    );
    setValidation(false);
  }
  if(profileResult == true)
  {
   console.log("inside ");
    navigation.navigate('ChooseInterestScreen',{ customer_id: customer_id })
  }
  useEffect(() => {
    // Geolocation.getCurrentPosition(info =>
    //   {
    //     console.log("here info:",info);
    //   });
    setcustomerid(customer_id);
  }, []);
  const backAction = () => {
    navigation.navigate('LoginScreen');
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backAction);

    return () =>
      BackHandler.removeEventListener("hardwareBackPress", backAction);
  }, []);

  return (
    <ScrollView
    showsVerticalScrollIndicator={false}
    >
    <View style={styles.container}>
      <View style={{flex:0.5}} >
      <LinearGradient
        start={{x: 0, y: 0.5}}
        end={{x: 1, y: 0.5}}
        colors={['#2A7E8D', '#140C56']}
        style={styles.linearGradient}>

        </LinearGradient>
        <View style={styles.elevatedContainer} >
            <Text style={styles.headigText} >Create Your Profile</Text>
            {/* <TouchableOpacity style={styles.prfileImageBtn} >
                <MaterialIcons name={"upgrade"} size={150} color={"#D9DBE9"}/>
                <Text style={[styles.headigText,{fontSize:18,color:"#6E7191"}]} >
                Upload Profile Image
                </Text>
            </TouchableOpacity> */}
        </View>
      </View>

        <View style={styles.formCoantainer}>
        <FormTextInput
           name="fullName"
           onChangeText={handleProfileFormChange}
           
           value={profileForm.fullName}
           keyboardType= "text"
           
            placeholder={'Eg. Mohammed James Wang'}
          />
             <FormTextInput
           name="locationCity"
            onChangeText={handleProfileFormChange}
          
            value={profileForm.locationCity}
            keyboardType= "text"
            // title={'City'}
            placeholder={'Enter City'}
          />
            <FormTextInput
            name="locationCountry"
            onChangeText={handleProfileFormChange}
            editable={false}
            value={country_name}
            keyboardType= "text"
            // title={'Country'}
            placeholder={'Enter Country'}
          />
          <TouchableOpacity style={{alignSelf: 'flex-end',flexDirection:"row",alignItems:"center",marginTop:5}}>
          {/* <EvilIcons name={"paperclip"} size={20} color={"#287287"}/> */}
            {/* <Text
              style={[
                styles.headigText,
                {fontSize: 13, lineHeight: 22, color: '#287287'},
              ]}>
              Upload Form of Identification
            </Text> */}
          </TouchableOpacity>
          <GradientButton 
      // onPress={() => navigation.navigate('ChooseInterestScreen')}
      onPress={handleProfileFormSubmit}
      btnText={'Save Profile'} />
      </View>
      
     {/* <GradientButton 
         
          onPress={() => navigation.navigate('ChooseInterestScreen',{ customer_id: customer_id })}
          btnText={'interest page'}
        /> */}
        {/* <GradientButton 
         
         onPress={() => navigation.navigate('ChooseInterestScreen',{ customer_id: "1" })}
         btnText={'interest page'}
       /> */}
    </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    height:windowHeight,
    backgroundColor: '#FFF',
  },
  linearGradient: {
    flex:0.8,
    borderBottomLeftRadius:46,
    borderBottomRightRadius:46
  },
  elevatedContainer:{
    position:"absolute",
    top:25,
    alignItems:"center",
    justifyContent:"space-around",
    width:"100%",
    height:"100%",
  },
  prfileImageBtn:{
      backgroundColor:"#F7F7FC",
      width:"80%",
      height:"80%",
      borderRadius:25,
      alignItems:"center",
      justifyContent:"center"

  },
  headigText: {
    fontSize: 24,
    lineHeight: 28.64,
    color: '#FCFCFC',
    fontWeight: '600',
    marginBottom:20
  },
  formCoantainer: {
    marginTop: 10,
    flex:1,padding:10
  },
});
