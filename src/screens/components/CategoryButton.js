import React, {useEffect} from 'react';
import {StyleSheet, TouchableOpacity, Text, View} from 'react-native';
import Colors from '../../constants/Colors';

const CategoryButton = ({icon, btnStyle = {}, label, isSelected,dressCodeCategoryId,selectedbutton,
  dressCodeId}) => {

   
  const [buttoncolor, setbuttoncolor] = React.useState('#ffffff');
  console.log("check here");
  console.log("selected dress code id:",dressCodeId);
  
  useEffect(() => {
    if(dressCodeId !="" )
    {
      if(dressCodeId!= dressCodeCategoryId)
   {
     setbuttoncolor('#ffffff');
   }
    }
   
  });
const handleChangeColor =()=>
{  

      if(buttoncolor=='#ffffff')
        {
          setbuttoncolor(Colors.violedBg);
        }
        else 
        {
          console.log("here");
          setbuttoncolor('#ffffff');
        }
      selectedbutton(dressCodeCategoryId);   
}




  return (
    <View style={styles.btnView}>
      <TouchableOpacity
        style={[
          styles.btnStyle,
          {
            backgroundColor:  buttoncolor,
          },
          btnStyle,
        ]}
        onPress={handleChangeColor}>
        {icon}
      </TouchableOpacity>
      <Text
        style={{
          marginTop: 5,
          fontSize: 12,
          fontWeight: '500',
          color: 'black',
        }}>
        {label}
      </Text>
    </View>
  );
};

export default CategoryButton;

const styles = StyleSheet.create({
  btnView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 7,
  },
  btnStyle: {
    height: 55.5,
    width: 55.5,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.violedBg,
  },
});
