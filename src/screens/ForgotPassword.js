import React, {useRef,useEffect,useState} from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Alert
} from 'react-native';
import FormTextInput from '../components/formInputs/FormTextInput';
import GradientButton from '../components/buttons/GradientButton';
import TransParentButton from '../components/buttons/TransParentButton';
import {useAuthService} from './../hooks/useAuthService';



const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default ForgotPassword  = ({navigation}) => {

    const { handleForgetPasssword,handleForgetPassswordChange,emailForgetPassword } = useAuthService();
  return (

    <ScrollView
      contentContainerStyle={{flexGrow: 1}}
      showsVerticalScrollIndicator={false}>

        {/* {emailValidation ? showAlert : null} */}
      <View style={styles.container}>
        <View style={{height: windowHeight * 0.2}}>
          <Image
            style={{
              height: '100%',
              width: '30%',
            }}
            source={require('../assets/momentz_final.png')}
          />
        </View>
        <View style={{flex: 0.75}}>
          <Text style={styles.headigText}>Forgot Password</Text>
          <Text style={[styles.headigText, {fontSize: 13, lineHeight: 22}]}>
            Please Provide Your Registered Email ID To Reset Your Password
          </Text>
          <View style={styles.formCoantainer}>
            <FormTextInput
              placeholder={'Email ID'}
              name="email"
              value={emailForgetPassword}
              onChangeText={handleForgetPassswordChange}
              keyboardType= "email-address"
            />
          </View>
        </View>
        <View>
          <GradientButton
            btnText={'Submit'}
            onPress={handleForgetPasssword}
          />
        </View>
      </View>
    </ScrollView>
   );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    padding: 20,
    backgroundColor: '#FFF',
  },
  headigText: {
    fontSize: 48,
    lineHeight: 50,
    color: '#14142B',
    fontWeight: '300',
  },
  formCoantainer: {
    marginTop: 20,
  },
});
