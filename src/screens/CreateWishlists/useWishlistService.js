import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import validator from 'validator'
import {
  wishlistFormService,
} from '../../services/authService';
import {
  Alert,
} from 'react-native'
// import {addUser} from './../redux/actions/userActions';
import RBSheet from 'react-native-raw-bottom-sheet';
export const useWishlistService = () => {
  const dispatch = useDispatch();
  const [loading, setLoading] = React.useState(false);
  const [nameValidation, setnameValidation] = React.useState(false);
  const [descriptionValidation, setdescriptionValidation] = React.useState(false);
  const [priceValidation, setpriceValidation] = React.useState(false);
  const [stockValidation, setstockValidation]= React.useState(false);
  const [validation, setvalidation] = React.useState(true);
  const [ticketResult, setticketResult] = React.useState(false);
  const refRBSheet = React.useRef();

  const [registerForm, setRegisterForm] = React.useState({
    giftName: '',
    itemTypeId: '',
    accountInfo: '',
    additionalInfo: '',
  });

  // const [registerForm, setRegisterForm] = React.useState({ });
  function validateName(name,text)
 {
        if(name=="name")
        {
                if(text.length < 10){
                console.log("Is name strong");
                setnameValidation(true);
              } else {
                console.log("Is Not Strong name");
                setnameValidation(false);
              }
        }
 }
 function validateDescriptionr(name,text)
 {
          if(name=="description")
          {
                  if(text.length < 10){
                  console.log("Is description strong");
                  setdescriptionValidation(true);
                } else {
                  console.log("Is Not Strong name");
                  setdescriptionValidation(false);
                }
              
        }
}
function validatePrice(name,text)
 {
          if(name=="price" )
          {
            if (isNaN(text)  ) {
              console.log("not valid number");
              setpriceValidation(false);
            }      
            else{
              setpriceValidation(true);
           
            }
        }
}
function validateStock(name,text)
 {
          if(name=="stock" )
          {
            if (isNaN(text )  ) {
              console.log("not aa valid number");
              setstockValidation(false);
            }      
            else{
              setstockValidation(true);
              // setstockValidation(true);
            }
        }
}


  const handleGiftNameChange = (name, text) => {
   console.log("text here:",text);
    
      setRegisterForm({
        ...registerForm,
        ["giftName"]: text,
      });
  };
  
  const handleAccountInfoChange = (name, text) => {
    console.log("text here:",text);
     
       setRegisterForm({
         ...registerForm,
         ["accountInfo"]: text,
       });
   };
const handleAdditionalInformationChange=(name,text)=>
{  console.log("text here:",text);
     
  setRegisterForm({
    ...registerForm,
    ["additionalInfo"]: text,
  });
};  


 const handleFormSubmitChange  = async (event,itemTypeId) => 
 {
   console.log("here is data",itemTypeId);
   registerForm.itemTypeId=itemTypeId;
   const eventTypeId="1";
   if(registerForm.giftName=="" || registerForm.accountInfo=="" || registerForm.additionalInfo=="")
   {
    console.log("not proper  valid ");
    showAlert("Validation Error!!");
    refRBSheet.current.close();
    return ;
   }
   console.log("form here:",registerForm);
    try {
      console.log("inside try");
      const response = await wishlistFormService(registerForm,eventTypeId);
      console.log("after wishlist form submit",response.data);
      showAlert("Sucessfully uploaded wishlist!!");
      refRBSheet.current.close();
    } catch (error) {
      console.log('Auth Eror --- ticket-------');
      console.log(error.message);
      refRBSheet.current.close();

    }
   
 }
 function showAlert(message)
 {
   Alert.alert(
     ""+message,
     "",
     [
       {},
       { text: "OK", onPress: () => console.log("OK Pressed") }
     ]
   );
 }
 
  return {
    loading,
    setLoading,
    handleFormSubmitChange,
    registerForm,
    validation,
    setvalidation,
    ticketResult,
    refRBSheet,
    handleGiftNameChange,
    handleAccountInfoChange,
    handleAdditionalInformationChange

  
  };
};
