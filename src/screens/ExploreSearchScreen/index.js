import React, {useRef,useEffect,useState} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import HeaderWithSearchbar from '../../components/Header/HeaderWithSearchbar';
import Categories from '../components/Categories';
import EventsForYou from './EventsForYou';
import NearbySection from './NearbySection';
import ViewAllEventsButton from './ViewAllEventsButton';
import CreateEventBtn from '../../components/buttons/CreateEventBtn';
import RBSheet from 'react-native-raw-bottom-sheet';
// import GradientButton from '../components/buttons/GradientButton';
import GradientButton from '../../components/buttons/GradientButton';

const ExploreSearchScreen = ({navigation}) => {
  const refRBSheet = useRef();
  const handleNavigationTo = (screenName,eventTypeId) =>
  {
    console.log("ebent ype id:",eventTypeId);
   
    refRBSheet.current.close();
    navigation.navigate(screenName);
  } 
  const SlideUpEventRBsheet =()=>
  {
    console.log("yes");
    refRBSheet.current.open();
  }
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <HeaderWithSearchbar
        title="Explore"
        onPressFilterBtn={() => handleNavigationTo('FilterScreen')}
      />
      <ScrollView>
        <EventsForYou
          onPressAllEvent={() => handleNavigationTo('ExploreEventSearchScreen')}
        />
        <Categories />
        <ViewAllEventsButton />
        
        <NearbySection navigation={navigation} />
      </ScrollView>
      {/* <CreateEventBtn onPress={()=>handleNavigationTo('EventCreationScreen')} /> */}
       <CreateEventBtn onPress={SlideUpEventRBsheet} />
       <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        height={250}
        closeOnPressMask={false}
        customStyles={{
          draggableIcon: {
            backgroundColor: 'blue',
          },
        }}>
        <Text   style={[styles.headingText, {color:'#000000'}]}>Choose Event Type</Text>
        <GradientButton onPress={()=>handleNavigationTo('EventCreationScreen','1')} btnText={'Public Event'} />
        <GradientButton onPress={()=>handleNavigationTo('EventCreationScreen','2')} btnText={'Private Event'} />
      </RBSheet>
    </View>
  );
};

export default ExploreSearchScreen;

const styles = StyleSheet.create({
  eventsView: {
    paddingLeft: 20,
    marginVertical: 20,
  },
  headingText: {
    fontSize: 24,
    lineHeight: 28,
    color: '#4E4B66',
    textAlign: 'center',
    marginTop: 20,
  },
});
