import {ADD_USER} from "../types.js";

export const addUser = payload  => ({
    type:ADD_USER,
    payload:payload
}) 

export const addCustomerId = payload  => ({
    type:ADD_CUSTOMERID,
    payload:payload
}) 