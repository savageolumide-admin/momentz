import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import validator from 'validator'
import {
    createProfileService,
    interestService
} from './../services/authService';
import {addUser} from './../redux/actions/userActions';
import { set } from 'react-native-reanimated';

export const useProfileService = () => {
  const dispatch = useDispatch();
  const [loading, setLoading] = React.useState(false);
 
  // const [loginForm, setLoginForm] = React.useState({
  //   identifier: 'johndoe@gmail.com',
  //   password: 'password',
  // });
  const [fullnameValidation, setfullnameValidation] = React.useState(false);
  const [cityValidation, setcityValidation] = React.useState(false);
  const [countryValidation, setcountryValidation] = React.useState(false);
  const [Validation, setValidation] = React.useState(false);
  const [profileResult, setprofileResult] = React.useState(false);
  const [profileForm, setprofileForm] = React.useState({ 
    // fullName: '',
    // locationCity: '',
    // locationCountry: '',
    
  });
  const [fullname, setfullname] = React.useState("");
  const [city, setcity] = React.useState("");
  const [country, setcountry] = React.useState("");
  const [customerid,setcustomerid]=React.useState("false");
  const [interest, setinterest] = React.useState([]);
  const [interestResult, setinterestResult] = React.useState(false);
  
  const handleProfileFormChange = (name, text) => {
    
  console.log(text);
   if(name == "fullName")
   { 
    setfullname(text);
        //    if(text == "")
        //    {
        //     setfullnameValidation(true);
        //    }
        //    else{
        //     setfullnameValidation(false); 
        //    }
   }
   if(name == "locationCity")
   {  setcity(text);
                // if(text == "")
                // {
                //     setcityValidation(true);
                // }
                // else{setcityValidation(false);  }
   }
  //  if(name == "locationCountry")
  //  {  setcountry(text);
  //               // if(text == "")
  //               // {
  //               //     setcountryValidation(true);
  //               // }
  //               // else{setcountryValidation(false);  }
  //  }
   
  
    setprofileForm({
      ...profileForm,
      [name]: text,
    });
   
  };
 
  

  const handleProfileFormSubmit = async () => {
    //setLoading(true);

 
    console.log("check:",fullname.length);
    console.log("check:",city.length);
    console.log("check:",country.length);
    
      if(fullname.length == 0 || city.length == 0  )
      {
        setValidation(true);
        console.log("validation problem");
      //  setLoading(false);
        return ;
      }
      console.log("profileForm",profileForm);
      console.log("customer id",customerid);

    try {
      const response = await createProfileService(profileForm,customerid);
      console.log("after profile",response.data);
      setprofileResult(true);
     
      
    } catch (error) {
      console.log('Profile Eror --- Profile -------');
      console.log(error.message);
      

    }
  };
  const handleInterestFormSubmit = async () => {

    console.log("interest",{categoryIds:interest});
    try {
        const response = await interestService(
            {categoryIds:interest},customerid
            );
        console.log("after interest",response.data);
        setinterestResult(true);
       setinterest([]);
        
      } catch (error) {
        console.log(' Eror --- Interest -------');
        console.log(error.message);
        
  
      }
  }
//   const [registerForm, setRegisterForm] = React.useState({
//     email: '',
//     password: '',
//     phoneNumber: '',
//     dob: '',
//   });
  // const [registerForm, setRegisterForm] = React.useState({ });
  

  
  return {
   
    handleProfileFormChange,
    handleProfileFormSubmit,
    profileForm,
    setcustomerid,
    profileResult,
    Validation,
    setValidation,
    handleInterestFormSubmit,
    interest,
    setinterest,
    interestResult
    
    
    
  };
};
