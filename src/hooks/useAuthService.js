import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import validator from 'validator'
import {
  loginService,
  registerUserService,
  requestOTPService,
  verifyOTPService,
  resetPasswordService
} from './../services/authService';
import {addUser} from './../redux/actions/userActions';

export const useAuthService = () => {
  const dispatch = useDispatch();
  const [loading, setLoading] = React.useState(false);
  const [emailValidation, setemailValidation] = React.useState(false);
  const [passwordValidation, setpasswordValidation] = React.useState(false);
  const [phonenumberValidation, setphonenumberValidation] = React.useState(false);
  const [dobValidation, setdobValidation] = React.useState(false);
  const [validation, setValidation] = React.useState(true);
  const [loginResult, setloginResult] = React.useState(false);
  const [signupResult, setsignupResult] = React.useState(true);
  const [openOTPScreen, setopenOTPScreen] = React.useState(false);
  const [invalidOtp,setinvalidOtp]=React.useState(false);
  const [isOtpVerified,setisOtpVerified]=React.useState(false);
  const [otpformatRight,setotpformatRight]=React.useState(false);
  const [otpVerify, setOtpVerify] = React.useState(false);
  const [otp, setOtp] = React.useState('');
  const [customerId, setcustomerId] = React.useState("");
  // const [loginForm, setLoginForm] = React.useState({
  //   identifier: 'johndoe@gmail.com',
  //   password: 'password',
  // });
  const [countrycode, setcountrycode] = React.useState();
  const [countryname, setcountryname] = React.useState();
  const [loginForm, setLoginForm] = React.useState({ });
  const [emailForgetPassword, setemailForgetPassword] = React.useState("");
  let today = new Date().toISOString().slice(0, 10);
  const [date, setDate] = React.useState(today);

  const handleLoginFormChange = (name, text) => {
    
    validateEmail(name,text);
    validatePassword(name,text);
   console.log("name and text:",name +""+ text)
  
    setLoginForm({
      ...loginForm,
      [name]: text,
    });
   
  };
 function validateEmail(name,text)
 {
    if(name=="identifier" || name=="email")
        {
            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
            if (reg.test(text) === false) {
              console.log("Email is Not Correct");
              // this.setState({ email: text })
              setemailValidation(false);
            
            }
            else {
              setemailValidation(true);
              console.log("Email is Correct");
            }
        }
 }
 function validatePassword(name,text)
 {
        if(name=="password")
        {
                if(text.length > 5){
                // setErrorMessage('Is Strong Password')
                console.log("Is Strong Password");
                setpasswordValidation(true);
              } else {
                console.log("Is Not Strong Password");
                // setErrorMessage('Is Not Strong Password')
                setpasswordValidation(false);
              }
        }
 }
 function validatePhoneNumber(name,text)
    {
           
      if(name == "phoneNumber")  
      {
        const isValidPhoneNumber = validator.isMobilePhone(text);
        if(isValidPhoneNumber)
        { console.log("right phone");
          setphonenumberValidation(true);
        }
        else{
          console.log("wrong phone");
          setphonenumberValidation(false);
        }
      }

    }
 function validatedob(name,text)
    {
      // var bday=text;
       if(name == 'dob')   
       {
       var bday="1990-12-31";
        bday=bday.split("-");
        var bday_in_milliseconds = new Date(parseInt(bday[2], 10), parseInt(bday[1], 10) - 1 , parseInt(bday[0]), 10).getTime(); //birth-date in milliseconds
        var now = new Date().getTime(); //current time in milliseconds
        if(now - bday_in_milliseconds > 410000000000){ //567648000000 is 18 years in milliseconds
            // 13+
            console.log("above 13 ");
            setdobValidation(true);
        }
        else{
            // under 13
            console.log("below 13 ");
            setdobValidation(false);
        }
       }

    }

  

  const handleLoginFormSubmit = async () => {
    setLoading(true);
      
      if(emailValidation == false || passwordValidation == false)
      {
        setValidation(false);
        console.log("not valid email");
        setLoading(false);
        return ;
      }
    try {
      const response = await loginService(loginForm);
      console.log("after login",response.data);
      setloginResult(false);
      const {profile, accessToken} = response.data;
      setTimeout(() => {
        setLoading(true);
        dispatch(
          addUser({
            profile,
            token: accessToken,
          }),
        );
      }, 1000);
    } catch (error) {
      console.log('Auth Eror --- Login -------');
      console.log(error.message);
      setLoading(false);
      setloginResult(true);

    }
  };

  const [registerForm, setRegisterForm] = React.useState({
    email: '',
    password: '',
    phoneNumber: '',
    dob: '',
  });

  // const [registerForm, setRegisterForm] = React.useState({ });
  

  const handleRegisterFormChange = (name, text) => {

    validateEmail(name,text);
    validatePassword(name,text);
    validatePhoneNumber(name,text);
    //validatedob(name,text);

    
      setRegisterForm({
        ...registerForm,
        [name]: text,
      });
    
    
    
  };
  const handleRegisterFormSubmit = async () => {
    console.log("country name",countryname);
    console.log("country code",countrycode);
    
    
    if(countryname =="Select Country Code" || countrycode==undefined )
    {
      setValidation(false);
      console.log("validation fails in 1st step");
      setLoading(false);
      return ;
    }
    
     const date_input=date;
     const phone=countrycode.concat(registerForm.phoneNumber);
     console.log("registerForm ",phone);
    
    const newform={
      ...registerForm,
        ["phoneNumber"]: phone,
        ["dob"]: date_input,
    }
    
    setLoading(true);
 
    console.log("before api call",newform);
    if(emailValidation == false || passwordValidation == false || phonenumberValidation == false )
    {
      setValidation(false);
      console.log("validation fails in 2nd step");
      setLoading(false);
      return ;
    }
   
    try {
      const response = await registerUserService(newform);
      setsignupResult(true);
      setopenOTPScreen(true);
      

      console.log("after signup",response.data);
      setcustomerId(response.data.customerId);
      

      setTimeout(() => {
        setLoading(false);
      }, 2000);
    } catch (error) {
      console.log('Auth Eror --- Register -------');
      console.log(error.message);
      setsignupResult(false);
    }
  };
  console.log("otp here:",otp);
  console.log("customerid here:",customerId);

  
  const handleRequestOTP = async () => {
    try {
      const response = await requestOTPService({
        email: registerForm.email,
      });
      return 'OTP Request';
    } catch (error) {
      console.log('OTP Error --- Request OTP -------');
      console.log(error.message);
      return null;
    }
  };

  const handleVerifyOTP = async () => {

    console.log("before verifying otp:",registerForm.email +" "+ otp)
    try {
      const response = await verifyOTPService({
        email: registerForm.email,
        otp:otp,
      });
     // setisOtpVerified(true);
      setotpformatRight(true);
      console.log("after verify otp",response.data);
    //  navigation.navigate('CreateProfileScreen');    
      //return response;
    } catch (error) {
      console.log('OTP Error --- Request OTP -------');
      console.log(error.message);
      setinvalidOtp(true);
    }
  };
  
  const handleForgetPassswordChange = (name, text) => {
    console.log("handleForgetPassswordChange:",text)
    if(name=="email")
    {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
        if (reg.test(text) === false) {
          console.log("Email is Not Correct");
          // this.setState({ email: text })
          setemailValidation(false);
        
        }
        else {
          setemailValidation(true);
          console.log("Email is Correct");
          setemailForgetPassword(text);
        }
    }
    
   console.log("name and text:", text)
  
  


   
  };
  const handleForgetPasssword = async () => {

        console.log("before using reset api:",emailForgetPassword)

        if(emailValidation == false )
        {
          setValidation(false);
          console.log("not valid email");
          return ;
        }
        try {
          const response = await resetPasswordService({
            email: emailForgetPassword,
          });
        

        } catch (error) {
          console.log('Reset error --- Reset Password Error-------');
          console.log(error.message);
        
        }
    
  };
  if(otp.length == '6' &&  otpformatRight == false)
  {

    handleVerifyOTP();
  }

  return {
    loading,
    setLoading,
    validation,
    loginResult,
    signupResult,
    setsignupResult,
    setloginResult,
    setValidation,
    loginForm,
    handleLoginFormChange,
    handleLoginFormSubmit,
    openOTPScreen,
    otp,
    setOtp,
    setinvalidOtp,
    invalidOtp,
    isOtpVerified,
    setopenOTPScreen,
    registerForm,
    otpformatRight,
    setRegisterForm,
    setotpformatRight,
    customerId,
    handleRegisterFormChange,
    handleRegisterFormSubmit,
    handleRequestOTP,
    handleVerifyOTP,
    handleForgetPassswordChange,
    handleForgetPasssword,
    emailForgetPassword,
    setcountrycode,
    setcountryname,
    countryname,
    date,
    setDate
  };
};
