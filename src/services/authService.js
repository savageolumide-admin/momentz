import {createAPIConfig} from './apiConfig';
const apiConfig = createAPIConfig();

export const loginService = async formData => {
  console.log("loginForm:",formData);
  return await apiConfig.post('/auth/login', formData);
};

export const registerUserService = async formData => {
  console.log("registerForm:",formData);
  return await apiConfig.post('/customers/register', formData);
};

export const requestOTPService = async formData => {
  return await apiConfig.post('/customers/request-otp', formData);
};

export const verifyOTPService = async formData => {
  return await apiConfig.post('/customers/verify-otp', formData);
};


export const resetPasswordService = async formData => {

  console.log("resetPasswordService:",formData);
  return await apiConfig.put('/password/reset', formData);
};
export const interestService = async (formData,customerId) => {
  // var customerId='101';
  console.log("interestForm:",formData);
  console.log("customeridFormInterestService:",customerId );
  const url='/customers/'+customerId+'/interests';
  console.log(url);
  return await apiConfig.put(url, formData);
};

export const createProfileService = async (profileForm ,customerId) => {
  console.log("profileFormInService:",profileForm );
  console.log("customeridFormInService:",customerId );
 const url='/customers/'+customerId+'/profiles';
 console.log("UrlFormInService:",url);
  return await apiConfig.patch(url, profileForm);
};

export const logoutService = async formData => {
  return await apiConfig.post('/auth/logout', formData);
};

//create events
export const ticketFormService = async (formData,eventTypeId) => {
  console.log("Form:",formData);
  console.log("event id:",eventTypeId );
  const url='/events/'+eventTypeId+'/tickets';
  console.log(url);
  return await apiConfig.post(url, formData);
};

export const dresscodeFormService = async (formData,eventTypeId) => {
  console.log("Form:",formData);
  console.log("event id:",eventTypeId );
  const url='/events/'+eventTypeId+'/dress-code';
  console.log(url);
  return await apiConfig.post(url, formData);
};

export const wishlistFormService = async (formData,eventTypeId) => {
  console.log("Form:",formData);
  console.log("event id:",eventTypeId );
  const url='/events/'+eventTypeId+'/wishlist';
  console.log(url);
  return await apiConfig.post(url, formData);
};