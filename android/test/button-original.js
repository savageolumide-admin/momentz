import React, {useRef,useEffect} from 'react';
import {Text, View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import { color } from 'react-native-reanimated';
import EvilIcons from 'react-native-vector-icons/dist/EvilIcons';

// function CircularButton({
//   radius = 50,
//   margins = {},
//   bgcolor,
//   textColor = '#FCFCFC',
//   btnText = 'Text',
// }) {
  function CircularButton(props) {
    // const [bgcolor, setbgcolor] = React.useState('');
    
const margins={};
const textColor = '#FCFCFC';
const  btnText = 'Text';
const  {bgcolor}  = props.backcolor;
const radius = props.radius;

console.log("props here:",radius);
// useEffect(() => {
//    setbgcolor(props.backcolor+"");
// }, []);
// console.log(bgcolor);
  return (
    <TouchableOpacity
      style={[
        styles.btnContainer,
        {
          height: 100,
          width: 100,
          borderRadius: 100,
          backgroundColor: `${bgcolor}`,

        },
       
      ]}>
      <EvilIcons name={'image'} size={30} color={textColor}/>

      <Text style={[styles.buttonText, {color: textColor}]}>{btnText}</Text>
    </TouchableOpacity>
  );
}

export default CircularButton;

const styles = StyleSheet.create({
  btnContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 18,
    lineHeight: 34,
    textAlign: 'center',
    color: '#287287',
  },
});
